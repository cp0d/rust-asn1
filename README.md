# ASN1

_Abstract Syntax Notation One_ (ASN.1) is a powerful language for the definition of abstract objects.
It is accompanied by encoding rules to translate the defined objects into easily processable binary representations.
ASN.1 sees wide usage e.g. in the definition of network protocol standards.
A good starting point into ASN.1 and its encodings is e.g. [A Layman's Guide to a Subset of ASN.1, BER, and DER](http://luca.ntop.org/Teaching/Appunti/asn1.html).

This library provides functionality to model ASN.1-defined objects in Rust, as well as the encoding into and decoding from their binary representation (only DER is supported, though).

At the core of this library stands the **ASN1** enum.
Its variants represent the available ASN.1 data types.
Using the *encode_der()* method, an ASN.1 object can be serialized into its binary DER prepresenation.
The associated function *from_der()* creates (nested) ASN1 enum variant form a u8 vector containing a DER encoded binary ASN.1 object.

---

Contact: [praty.code@0xaa55.org](mailto:praty.code@0xaa55.org) or join via Matrix: #praty_code:0xaa55.org
