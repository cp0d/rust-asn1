extern crate bigint;
extern crate chrono;

use bigint::uint::U512;
use chrono::prelude::*;
use std::{str, fmt};

pub const ASN1_CLASS_UNIVERSAL: u8 = 0x0;
pub const ASN1_CLASS_APPLICATION: u8 = 0x1;
pub const ASN1_CLASS_CONTEXTSPECIFIC: u8 = 0x2;
pub const ASN1_CLASS_PRIVATE: u8 = 0x3;

pub const ASN1_TYPE_BOOLEAN: u8 = 0x1;
pub const ASN1_TYPE_INTEGER: u8 = 0x2;
pub const ASN1_TYPE_BITSTRING: u8 = 0x3;
pub const ASN1_TYPE_OCTETSTRING: u8 = 0x4;
pub const ASN1_TYPE_NULL: u8 = 0x5;
pub const ASN1_TYPE_OBJECTIDENTIFIER: u8 = 0x6;
pub const ASN1_TYPE_UTF8STRING: u8 = 0xC;
pub const ASN1_TYPE_SEQUENCE: u8 = 0x10;
pub const ASN1_TYPE_SET: u8 = 0x11;
pub const ASN1_TYPE_NUMERICSTRNG: u8 = 0x12;
pub const ASN1_TYPE_PRINTABLESTRING: u8 = 0x13;
pub const ASN1_TYPE_T61STRING: u8 = 0x14;
pub const ASN1_TYPE_IA5STRING: u8 = 0x16;
pub const ASN1_TYPE_UTCTIME: u8 = 0x17;

// resticted type definitions
const ASN1_RESTRICTED_CHARACTER_STRING_PRINTABLE: [char; 74] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '\'', '(', ')', '+', ',', '-', '.', '/', ':', '=', '?'];
//const ASN1_RESTRICTED_CHARACTER_STRING_NUMERIC: [char; 11] = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ' ];
const ASN1_RESTRICTED_CHARACTER_STRING_T61: [char; 220] = [ '\u{0}', '\u{1}', '\u{2}', '\u{3}', '\u{4}', '\u{5}', '\u{6}', '\u{7}', '\u{8}', '\u{9}', '\u{a}', '\u{b}', '\u{c}', '\u{d}', '\u{e}', '\u{f}', '\u{10}', '\u{11}', '\u{12}', '\u{13}', '\u{14}', '\u{15}', '\u{16}', '\u{17}', '\u{18}', '\u{19}', '\u{1a}', '\u{1b}', '\u{1c}', '\u{1d}', '\u{1e}', '\u{1f}', '\u{20}', '\u{21}', '\u{22}', '\u{23}', '\u{24}', '\u{25}', '\u{26}', '\u{27}', '\u{28}', '\u{29}', '\u{2a}', '\u{2b}', '\u{2c}', '\u{2d}', '\u{2e}', '\u{2f}', '\u{30}', '\u{31}', '\u{32}', '\u{33}', '\u{34}', '\u{35}', '\u{36}', '\u{37}', '\u{38}', '\u{39}', '\u{3a}', '\u{3b}', '\u{3c}', '\u{3d}', '\u{3e}', '\u{3f}', '\u{40}', '\u{41}', '\u{42}', '\u{43}', '\u{44}', '\u{45}', '\u{46}', '\u{47}', '\u{48}', '\u{49}', '\u{4a}', '\u{4b}', '\u{4c}', '\u{4d}', '\u{4e}', '\u{4f}', '\u{50}', '\u{51}', '\u{52}', '\u{53}', '\u{54}', '\u{55}', '\u{56}', '\u{57}', '\u{58}', '\u{59}', '\u{5a}', '\u{5b}', '\u{5d}', '\u{5f}', '\u{61}', '\u{62}', '\u{63}', '\u{64}', '\u{65}', '\u{66}', '\u{67}', '\u{68}', '\u{69}', '\u{6a}', '\u{6b}', '\u{6c}', '\u{6d}', '\u{6e}', '\u{6f}', '\u{70}', '\u{71}', '\u{72}', '\u{73}', '\u{74}', '\u{75}', '\u{76}', '\u{77}', '\u{78}', '\u{79}', '\u{7a}', '\u{7c}', '\u{7f}', '\u{80}', '\u{81}', '\u{82}', '\u{83}', '\u{84}', '\u{85}', '\u{86}', '\u{87}', '\u{88}', '\u{89}', '\u{8a}', '\u{8b}', '\u{8c}', '\u{8d}', '\u{8e}', '\u{8f}', '\u{90}', '\u{91}', '\u{92}', '\u{93}', '\u{94}', '\u{95}', '\u{96}', '\u{97}', '\u{98}', '\u{99}', '\u{9a}', '\u{9b}', '\u{9c}', '\u{9d}', '\u{9e}', '\u{9f}', '\u{a0}', '\u{a1}', '\u{a2}', '\u{a3}', '\u{a4}', '\u{a5}', '\u{a7}', '\u{aa}', '\u{ab}', '\u{b0}', '\u{b1}', '\u{b2}', '\u{b3}', '\u{b5}', '\u{b6}', '\u{b7}', '\u{ba}', '\u{bb}', '\u{bc}', '\u{bd}', '\u{be}', '\u{bf}', '\u{c6}', '\u{d0}', '\u{d7}', '\u{d8}', '\u{de}', '\u{df}', '\u{e6}', '\u{f0}', '\u{f7}', '\u{f8}', '\u{fe}', '\u{111}', '\u{126}', '\u{127}', '\u{131}', '\u{132}', '\u{133}', '\u{138}', '\u{13f}', '\u{140}', '\u{141}', '\u{142}', '\u{149}', '\u{14a}', '\u{14b}', '\u{152}', '\u{153}', '\u{166}', '\u{167}', '\u{300}', '\u{301}', '\u{302}', '\u{303}', '\u{304}', '\u{306}', '\u{307}', '\u{308}', '\u{30A}', '\u{30B}', '\u{30C}', '\u{327}', '\u{328}', '\u{332}', '\u{2126}' ];
const ASN1_RESTRICTED_CHARACTER_STRING_IA5: [char; 128] = ['\u{0}', '\u{1}', '\u{2}', '\u{3}', '\u{4}', '\u{5}', '\u{6}', '\u{7}', '\u{8}', '\u{9}', '\u{a}', '\u{b}', '\u{c}', '\u{d}', '\u{e}', '\u{f}', '\u{10}', '\u{11}', '\u{12}', '\u{13}', '\u{14}', '\u{15}', '\u{16}', '\u{17}', '\u{18}', '\u{19}', '\u{1a}', '\u{1b}', '\u{1c}', '\u{1d}', '\u{1e}', '\u{1f}', '\u{20}', '\u{21}', '\u{22}', '\u{23}', '\u{24}', '\u{25}', '\u{26}', '\u{27}', '\u{28}', '\u{29}', '\u{2a}', '\u{2b}', '\u{2c}', '\u{2d}', '\u{2e}', '\u{2f}', '\u{30}', '\u{31}', '\u{32}', '\u{33}', '\u{34}', '\u{35}', '\u{36}', '\u{37}', '\u{38}', '\u{39}', '\u{3a}', '\u{3b}', '\u{3c}', '\u{3d}', '\u{3e}', '\u{3f}', '\u{40}', '\u{41}', '\u{42}', '\u{43}', '\u{44}', '\u{45}', '\u{46}', '\u{47}', '\u{48}', '\u{49}', '\u{4a}', '\u{4b}', '\u{4c}', '\u{4d}', '\u{4e}', '\u{4f}', '\u{50}', '\u{51}', '\u{52}', '\u{53}', '\u{54}', '\u{55}', '\u{56}', '\u{57}', '\u{58}', '\u{59}', '\u{5a}', '\u{5b}', '\u{5c}', '\u{5d}', '\u{5e}', '\u{5f}', '\u{60}', '\u{61}', '\u{62}', '\u{63}', '\u{64}', '\u{65}', '\u{66}', '\u{67}', '\u{68}', '\u{69}', '\u{6a}', '\u{6b}', '\u{6c}', '\u{6d}', '\u{6e}', '\u{6f}', '\u{70}', '\u{71}', '\u{72}', '\u{73}', '\u{74}', '\u{75}', '\u{76}', '\u{77}', '\u{78}', '\u{79}', '\u{7a}', '\u{7b}', '\u{7c}', '\u{7d}', '\u{7e}', '\u{7f}'];

//
// ASN-1 data types
//

/// ASN.1 objects
#[derive(Debug)]
pub enum ASN1 {
    Boolean { value: bool },
    Integer { value: U512 },
    BitString { /** in bits */ length: usize, value: Vec<u8> },
    OctetString { value: Vec<u8> },
    Null,
    ObjectIdentifier { value: Vec<u64> },
    UTF8String { value: String },
    Sequence { value: Vec<ASN1> }, // ordered
    Set { value: Vec<ASN1> }, // unordered
    PrintableString { value: String },
    T61String { value: String },
    IA5String { value: String }, // ASCII
    UTCTime { value: DateTime<Utc> },
    ContextSpecific { tag: u8, constructed: bool, value: Vec<u8> }, // contrains raw bytes from DER
    Application { tag: u8, constructed: bool, value: Vec<u8> }, // contrains raw bytes from DER
    Private { tag: u8, constructed: bool, value: Vec<u8> }, // contrains raw bytes from DER
    Unknown { class: u8, tag: u8, constructed: bool, value: Vec<u8> }, // contrains raw bytes from DER
}

impl ASN1 {
    fn restricted_string(value: String, restricted_set: &[char]) -> Result<String, EncodingError> {
        for c in value.chars() {
            if restricted_set.contains(&c) == false {
                return Err(EncodingError::InvalidCharacter{ character: c });
            }
        }
        Ok(value)
    }

    /*
     * Boolean
     */
    /* contructor */
    pub fn boolean(value: bool) -> Result<Self, ()> {
        Ok(ASN1::Boolean{ value: value })
    }

    /* value */
    /**
     * returns boolean value if object is of type ASN1::Boolean
     */
    pub fn boolean_value(self) -> Option<bool> {
        if let ASN1::Boolean { value } = self {
            return Some(value);
        }
        return None;
    }
    
    /*
     * Integer
     */
    /* contructor */
    pub fn integer(value: U512) -> Result<Self, ()> {
        Ok(ASN1::Integer{ value: value })
    }
    pub fn integer_from_u8(value: u8) -> Result<Self, ()> {
        Ok(ASN1::Integer{ value: U512::from(value) })
    }
    pub fn integer_from_u16(value: u16) -> Result<Self, ()> {
        Ok(ASN1::Integer{ value: U512::from(value) })
    }
    pub fn integer_from_u32(value: u32) -> Result<Self, ()> {
        Ok(ASN1::Integer{ value: U512::from(value) })
    }
    pub fn integer_from_u64(value: u64) -> Result<Self, ()> {
        Ok(ASN1::Integer{ value: U512::from(value) })
    }

    /* value */
    /**
     * returns integer value (U512) if object is of type ASN1::Integer
     */
    pub fn integer_value(self) -> Option<U512> {
        if let ASN1::Integer { value } = self {
            return Some(value);
        }
        return None;
    }

    /*
     * BitString
     */
    /* contructor */
    pub fn bitstring(mut value: Vec<u8>, unused: u8) -> Result<Self, EncodingError> {
        if unused > 7 {
            return Err(EncodingError::InvalidPaddingLength{ padding_length: unused as usize});
        }
        // set unused bits to 0
        let l = value.len() - 1;
        value[l] &= 0xFF << unused;
        Ok(ASN1::BitString{ length: value.len() * 8 - unused as usize, value })
    }
    pub fn bitstring_from_bitstring(value: String) -> Result<Self, EncodingError> {
        let mut octets = Vec::<u8>::new();
        let mut v = 0;
        let mut s = 7;
        for c in value.chars() {
            let c_int = u8::from_str_radix(&c.to_string(), 2);
            if c_int.is_err() {
                return Err(EncodingError::InvalidToken{ value: c, allowed: vec![ '0', '1' ] });
            }
            v += c_int.unwrap() << s;
            if s == 0 {
                octets.push(v);
                v = 0;
                s = 7;
            } else {
                s -= 1;
            }
        }
        if v > 0 {
            octets.push(v);
        }
        Ok(ASN1::BitString{ length: octets.len() * 8 - (s + 1) % 8, value: octets })
    }

    /* value */
    /**
     * returns bitstring as u8 vector if object is of type ASN1::BitString
     */
    pub fn bitstring_value(self) -> Option<Vec<u8>> {
        if let ASN1::BitString { value, .. } = self {
            return Some(value);
        }
        return None;
    }

    /* length in bits */
    pub fn bitstring_length(&self) -> Option<usize> {
        if let ASN1::BitString { length, .. } = *self {
            return Some(length);
        }
        return None;
    }

    /*
     * OctetString
     */
    /* contructor */
    pub fn octetstring(value: Vec<u8>) -> Result<Self, ()> {
        Ok(ASN1::OctetString{ value: value })
    }

    /* value */
    /**
     * returns octet string as u8 vector if object is of type ASN1::OctetString
     */
    pub fn octetstring_value(self) -> Option<Vec<u8>> {
        if let ASN1::OctetString { value } = self {
            return Some(value);
        }
        return None;
    }

    /*
     * Null
     */
    /* contructor */
    pub fn null() -> Result<Self,()> {
        Ok(ASN1::Null)
    }

	/*
	 * ObjectIdentifier
	 */
    /* contructor */
    pub fn objectidentifier(value: Vec<u64>) -> Result<Self, EncodingError> {
        match value[0] {
            0 | 1 => {
                if value[1] > 39 {
                    return Err(EncodingError::InvalidOIDSecondLevel{ root: value[0], second: value[1] });
                }
            },
            2 => {
                if value[1] > 47 {
                    return Err(EncodingError::InvalidOIDSecondLevel{ root: 2, second: value[1] });
                }
            }
            _ => {
                return Err(EncodingError::InvalidOIDRoot{ root: value[0] });
            }
        }
        Ok(ASN1::ObjectIdentifier{ value: value })
    }
    
    /* value */
    /**
     * returns object identifier as u64 vector if object is of type ASN1::ObjectIdentifier
     */
    pub fn objectidentifier_value(self) -> Option<Vec<u64>> {
        if let ASN1::ObjectIdentifier { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * UTF8String
	 */
    /* contructor */
    pub fn utf8string(value: String) -> Result<Self, ()> {
        Ok(ASN1::UTF8String{ value: value })
    }
    
    /* value */
    /**
     * returns string value if object is of type ASN1::UTF8String
     */
    pub fn uft8string_value(self) -> Option<String> {
        if let ASN1::UTF8String { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * Sequence
	 */
    /* contructor */
    pub fn sequence(value: Vec<ASN1>) -> Result<Self, ()> {
        Ok(ASN1::Sequence{ value: value })
    }
    
    /* value */
    /**
     * returns vector of ASN1 objects if object is of type ASN1::Sequence
     */
    pub fn sequence_value(self) -> Option<Vec<ASN1>> {
        if let ASN1::Sequence { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * Set
	 */
    /* contructor */
    pub fn set(value: Vec<ASN1>) -> Result<Self, ()> {
        Ok(ASN1::Set{ value: value })
    }
    
    /* value */
    /**
     * returns vector of ASN1 objects if object is of type ASN1::Set
     */
    pub fn set_value(self) -> Option<Vec<ASN1>> {
        if let ASN1::Set { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * PrintableString
	 */
    /* contructor */
    pub fn printablestring(value: String) -> Result<Self, EncodingError> {
        Ok(ASN1::PrintableString{ value: ASN1::restricted_string(value, &ASN1_RESTRICTED_CHARACTER_STRING_PRINTABLE)? })
    }
    
    /* value */
    /**
     * returns string value if object is of type ASN1::PrintableString
     */
    pub fn printablestring_value(self) -> Option<String> {
        if let ASN1::PrintableString { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * T61String
	 */
    /* contructor */
    #[allow(non_snake_case)]
    pub fn t61String(value: String) -> Result<Self, EncodingError> {
        Ok(ASN1::T61String{ value: ASN1::restricted_string(value, &ASN1_RESTRICTED_CHARACTER_STRING_T61)? })
    }
    
	/*
	 * IA5String
	 */
    /* contructor */
    #[allow(non_snake_case)]
    pub fn ia5String(value: String) -> Result<Self, EncodingError> {
        Ok(ASN1::IA5String{ value: ASN1::restricted_string(value, &ASN1_RESTRICTED_CHARACTER_STRING_IA5)? })
    }
    
	/*
	 * UTCTime
	 */
    /* contructor */
    pub fn utctime(value: DateTime<Utc>) -> Result<Self, ()> {
        Ok(ASN1::UTCTime{ value: value })
    }
    
    /* value */
    /**
     * returns UTC DateTime value if object is of type ASN1::UTCTime
     */
    pub fn utctime_value(self) -> Option<DateTime<Utc>> {
        if let ASN1::UTCTime { value } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * ContextSpecific
	 */
    /* contructor */
    pub fn contextspecific(tag: u8, constructed: bool, value: Vec<u8>) -> Result<Self, ()> {
        Ok(ASN1::ContextSpecific{ tag: tag, constructed: constructed, value: value })
    }
    
    /* value */
    /**
     * returns DER-encoded ASN.1 message if object is of type ASN1::ContextSpecific
     */
    pub fn contectspecific_value(self) -> Option<Vec<u8>> {
        if let ASN1::ContextSpecific { value, .. } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * Application
	 */
    /* contructor */
    pub fn application(tag: u8, constructed: bool, value: Vec<u8>) -> Result<Self, ()> {
        Ok(ASN1::Application{ tag: tag, constructed: constructed, value: value })
    }
    
    /* value */
    /**
     * returns DER-encoded ASN.1 message if object is of type ASN1::Application
     */
    pub fn application_value(self) -> Option<Vec<u8>> {
        if let ASN1::Application { value, .. } = self {
            return Some(value);
        }
        return None;
    }

	/*
	 * Private
	 */
    /* contructor */
    pub fn private(tag: u8, constructed: bool, value: Vec<u8>) -> Result<Self, ()> {
        Ok(ASN1::Private{ tag: tag, constructed: constructed, value: value })
    }
    
    /* value */
    /**
     * returns DER-encoded ASN.1 message if object is of type ASN1::Private
     */
    pub fn private_value(self) -> Option<Vec<u8>> {
        if let ASN1::Private { value, .. } = self {
            return Some(value);
        }
        return None;
    }

    /*
     * Unknown
     */

    /* value */
    /**
     * returns DER-encoded ASN.1 message if object is of type ASN1::Unknown
     */
    pub fn unknown_value(self) -> Option<Vec<u8>> {
        if let ASN1::Unknown { value, .. } = self {
            return Some(value);
        }
        return None;
    }

    /**
     * Parser for DER-encoded ASN.1 messages
     *
     * parses a vector of octets which represent a DER-encoded ASN.1 object
     * the vector must _start_ with an ASN.1 object
     * this function consumes the slice of the vector containing the DER-encoded object
     * if an error occurs during decoding, the resulting state of the octet vector is undefined
     *
     * # Example
     * ```
     * use asn1::ASN1;
     *
     * let mut der = vec![ 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00 ];
     * let asn1_seq = ASN1::from_der(&mut der).unwrap();
     */
    pub fn from_der(input: &mut Vec<u8>) -> Result<Self, DecodingError> {
        // get class and tag
        let identifier = input.remove(0);
        let class = identifier >> 6;
        let tag = identifier & 0x1F;
        let constructed: bool = identifier & 0x20 == 0x20;
        // get length
        let mut length = input.remove(0) as usize; // must be mutable as in case of multi-byte length re-assingment is necessary
        if length > 0x80 {
            // length is encoded in multiple bytes ("long form")
            let mut n_len = length & 0x7F;
            length = 0;
            while n_len > 0 {
                n_len -= 1;
                length += (input.remove(0) as usize) << (8 * n_len);
            }
        }
        // check if we've still enough bytes to extract object of extracted length
        if length > input.len() {
            return Err(DecodingError::InvalidLength{ length: length, remaining: input.len() });
        }
        let mut der_value: Vec<u8> = input.drain(..length).collect();
        match class {
            ASN1_CLASS_UNIVERSAL => { 
                match tag {
                    ASN1_TYPE_BOOLEAN => {
                        if length != 1 {
                            return Err(DecodingError::TypeLengthMismatch{ class: class, tag: tag, length: length });
                        }
                        Ok(ASN1::Boolean{ value: der_value[0] != 0 })
                    },
                    ASN1_TYPE_INTEGER => {
                        if length > 64 {
                            // U512 can carry integers consisting of 64 u8 max.
                            return Err(DecodingError::IntegerTooLong{ length: length });
                        }
                        let i = U512::from_big_endian(&der_value);
                        Ok(ASN1::Integer{ value: i })
                    },
                    ASN1_TYPE_BITSTRING => {
                        // check unused bits
                        let unused = der_value.remove(0);
                        if unused > 7 {
                            return Err(DecodingError::InvalidPaddingLength{ padding_length: unused as usize });
                        }
                        // check if unused bits are set to 0
                        if unused > 0 {
                            if der_value[der_value.len()-1] & (0xFF >> unused) != 0 {
                                return Err(DecodingError::NonZeroPaddingBits{ padded_byte: der_value[der_value.len()-1], unused: unused });
                            }
                        }
                        // object length is given in bytes including 1 byte declaring unused
                        // bits
                        let bitlen = der_value.len() * 8 - unused as usize;
                        Ok(ASN1::BitString{ length: bitlen, value: der_value })
                    }
                    ASN1_TYPE_OCTETSTRING => Ok(ASN1::OctetString{ value: der_value }),
                    ASN1_TYPE_NULL => Ok(ASN1::Null),
                    ASN1_TYPE_OBJECTIDENTIFIER => {
                        let mut oid = Vec::<u64>::new();
                        oid.push((der_value[0] / 40) as u64);
                        let scnd = der_value.remove(0) as u64 - oid[0] * 40;
                        oid.push(scnd);
                        let mut iter = der_value.iter();
                        let mut num: u64 = 0;
                        while let Some(i) = iter.next() {
                            if *i == 0x80 {
                                // values are encoded base 128, big endian, as few digits as
                                // possible, most significant bit set to 1 for all but the last
                                // octet => 0x80 is illegal
                                return Err(DecodingError::InvalidObjectIdentifierFormat);
                            }
                            if *i & 0x80 == 0 {
                                num += *i as u64;
                                oid.push(num);
                                num = 0;
                            } else {
                                num += (*i & 0x7F) as u64;
                                num <<= 7;
                            }
                        }
                        if num != 0 {
                            // object identifier bytes did not end in null-leading octet
                            return Err(DecodingError::InvalidObjectIdentifierFormat);
                        }
                        Ok(ASN1::ObjectIdentifier{ value: oid })
                    }
                    ASN1_TYPE_UTF8STRING => {
                        let strvalue = String::from_utf8(der_value);
                        if strvalue.is_err() {
                            return Err(DecodingError::InvalidUTF8{ failposition: strvalue.unwrap_err().utf8_error().error_len() });
                        }
                        Ok(ASN1::UTF8String{ value: strvalue.unwrap() })
                    }
                    ASN1_TYPE_SEQUENCE | ASN1_TYPE_SET => {
                        let mut seq = Vec::<ASN1>::new();
                        while der_value.is_empty() == false {
                            let asn1 = ASN1::from_der(&mut der_value);
                            if asn1.is_err() {
                                return Err(asn1.unwrap_err());
                            }
                            seq.push(asn1.unwrap());
                        }
                        if tag == ASN1_TYPE_SEQUENCE {
                            Ok(ASN1::Sequence{ value: seq })
                        } else {
                            Ok(ASN1::Set{ value: seq })
                        }
                    }
                    ASN1_TYPE_PRINTABLESTRING => {
                        let strvalue = String::from_utf8(der_value);
                        if strvalue.is_err() {
                            return Err(DecodingError::InvalidUTF8{ failposition: strvalue.unwrap_err().utf8_error().error_len() });
                        }
                        Ok(ASN1::PrintableString{ value: strvalue.unwrap() })
                    }
                    ASN1_TYPE_T61STRING => {
                        let strvalue = String::from_utf8(der_value);
                        if strvalue.is_err() {
                            return Err(DecodingError::InvalidUTF8{ failposition: strvalue.unwrap_err().utf8_error().error_len() });
                        }
                        Ok(ASN1::T61String{ value: strvalue.unwrap() })
                    }
                    ASN1_TYPE_IA5STRING => {
                        let strvalue = String::from_utf8(der_value);
                        if strvalue.is_err() {
                            return Err(DecodingError::InvalidUTF8{ failposition: strvalue.unwrap_err().utf8_error().error_len() });
                        }
                        Ok(ASN1::IA5String{ value: strvalue.unwrap() })
                    }
                    ASN1_TYPE_UTCTIME => {
                        if length != 13 {
                            return Err(DecodingError::InvalidTimeFormat);
                        }
                        let datetimestr = str::from_utf8(&der_value);
                        if datetimestr.is_err() {
                            return Err(DecodingError::InvalidTimeFormat);
                        }
                        let datetime = chrono::Utc.datetime_from_str(datetimestr.unwrap(), "%y%m%d%H%M%SZ");
                        if datetime.is_err() {
                            return Err(DecodingError::InvalidTimeFormat);
                        }
                        Ok(ASN1::UTCTime{ value: datetime.unwrap() })
                    }
                    _ => {
                        Ok(ASN1::Unknown{ class: class, tag: tag, constructed: constructed, value: der_value })
                    }
                }
            },
            ASN1_CLASS_CONTEXTSPECIFIC => {
                Ok(ASN1::ContextSpecific{ tag: tag, constructed: constructed, value: der_value })
            },
            _ => { // Private or Application
                if class == ASN1_CLASS_APPLICATION { // Application
                    Ok(ASN1::Application{ tag: tag, constructed: constructed, value: der_value })
                }
                else { //Private
                    Ok(ASN1::Private{ tag: tag, constructed: constructed, value: der_value })
                }
            },
        } // match class
    }

    /**
     * Returns a ASN.1 object's class
     *
     * # Example
     * ```
     * use asn1::{ASN1, ASN1_CLASS_UNIVERSAL};
     *
     * let asn1_null = ASN1::null().unwrap();
     * assert_eq!(asn1_null.class(), ASN1_CLASS_UNIVERSAL);
     * ```
     **/
    pub fn class(&self) -> u8 {
        match *self {
            ASN1::Boolean { .. } | ASN1::Integer { .. } | ASN1::BitString { .. } | ASN1::OctetString { .. } | ASN1::ObjectIdentifier { .. } | ASN1::UTF8String { .. } | ASN1::Sequence { .. } | ASN1::Set { .. } | ASN1::PrintableString { .. } | ASN1::T61String { .. } | ASN1::IA5String { .. } | ASN1::UTCTime { .. } => ASN1_CLASS_UNIVERSAL,
            ASN1::Null => ASN1_CLASS_UNIVERSAL,
            ASN1::ContextSpecific { .. } => ASN1_CLASS_CONTEXTSPECIFIC,
            ASN1::Application { .. } => ASN1_CLASS_APPLICATION,
            ASN1::Private { .. } => ASN1_CLASS_PRIVATE,
            ASN1::Unknown { class, .. } => class,
        }
    }

    /**
     * Returns a ASN.1 object's class as human-readable string
     *
     * # Example
     * ```
     * use asn1::{ASN1, ASN1_CLASS_UNIVERSAL};
     *
     * assert_eq!(ASN1::class_as_str(ASN1_CLASS_UNIVERSAL), "UNIVERSAL");
     * ```
     **/
    pub fn class_as_str(class: u8) -> &'static str {
        match class {
            0x00 => "UNIVERSAL",
            0x01 => "APPLICATION",
            0x10 => "CONTEXT-SPECIFIC",
            0x11 => "PRIVATE",
            _ => "(invalid class ID)",
        }
    }

    /**
     * Returns a ASN.1 object's type tag
     *
     * # Example
     * ```
     * use asn1::{ASN1, ASN1_TYPE_NULL};
     *
     * let asn1_null = ASN1::null().unwrap();
     * assert_eq!(asn1_null.tag(), ASN1_TYPE_NULL);
     * ```
     **/
    pub fn tag(&self) -> u8 {
        match *self {
            ASN1::Boolean { .. } => ASN1_TYPE_BOOLEAN,
            ASN1::Integer { .. } => ASN1_TYPE_INTEGER,
            ASN1::BitString { .. } => ASN1_TYPE_BITSTRING,
            ASN1::OctetString { .. } => ASN1_TYPE_OCTETSTRING,
            ASN1::Null => ASN1_TYPE_NULL,
            ASN1::ObjectIdentifier { .. } => ASN1_TYPE_OBJECTIDENTIFIER,
            ASN1::UTF8String { .. } => ASN1_TYPE_UTF8STRING,
            ASN1::Sequence { .. } => ASN1_TYPE_SEQUENCE,
            ASN1::Set { .. } => ASN1_TYPE_SET,
            ASN1::PrintableString { .. } => ASN1_TYPE_PRINTABLESTRING,
            ASN1::T61String { .. } => ASN1_TYPE_T61STRING,
            ASN1::IA5String { .. } => ASN1_TYPE_IA5STRING,
            ASN1::UTCTime { .. } => ASN1_TYPE_UTCTIME,
            ASN1::ContextSpecific { tag, .. } => tag,
            ASN1::Application { tag, .. } | ASN1::Private { tag, .. } => tag,
            ASN1::Unknown { tag, .. } => tag,
        }
    }

    /**
     * Returns the DER-encoded representation of an ASN.1 object as u8 vector
     */
    pub fn encode_der(&self) -> Vec<u8> {
        match *self {
            ASN1::Boolean { value } => { 
                let t: u8;
                if value {
                    t = 0xff;
                } else {
                    t = 0x00;
                }
                vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_BOOLEAN, 0x01, t ]
            },
            ASN1::Integer { value } => {
                let len = ( value.bits() as f64 / 8 as f64 ).ceil() as usize;
                let mut intbytes: Vec<u8> = vec![0; 64];
                value.to_big_endian(&mut intbytes);
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_INTEGER ];
                ret.append(&mut ASN1::length_der(len));
                ret.append(&mut intbytes[64 - len..].to_vec());
                ret
            },
            ASN1::BitString { ref value, length } => {
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_BITSTRING ];
                ret.append(&mut ASN1::length_der(( length as f64 / 8 as f64 ).ceil() as usize + 1));
                ret.push((length / 8) as u8);
                ret.append(&mut value.to_vec());
                ret
            },
            ASN1::OctetString { ref value } => {
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_OCTETSTRING ];
                ret.append(&mut ASN1::length_der(value.len()));
                ret.append(&mut value.to_vec());
                ret
            }
            ASN1::Null => vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_NULL, 0x00 ],
            ASN1::ObjectIdentifier { ref value } => {
                let mut oid = Vec::<u8>::new();
                oid.push((value[0] * 40 + value[1]) as u8);
                for i in &(value[2..]) {
                    let mut c = *i;
                    if c > 127 {
                        // multi-octet value encoding
                        let mut value_octets = Vec::<u8>::new();
                        while c > 127 {
                            value_octets.push((c % 128) as u8);
                            c >>= 7;
                        }
                        value_octets.push(c as u8);
                        while let Some(n) = value_octets.pop() {
                            if value_octets.is_empty() {
                                oid.push(n);
                            } else {
                                oid.push(n | 0x80);
                            }
                        }
                    } else {
                        oid.push(*i as u8);
                    }
                }
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_OBJECTIDENTIFIER ];
                ret.append(&mut ASN1::length_der(oid.len()));
                ret.append(&mut oid);
                ret
            },
            ASN1::UTF8String { ref value } => {
                let mut utf8_bytes = value.to_string().into_bytes();
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_UTF8STRING ];
                ret.append(&mut ASN1::length_der(utf8_bytes.len()));
                ret.append(&mut utf8_bytes);
                ret
            }
            ASN1::Sequence { ref value } | ASN1::Set { ref value } => {
                let mut seq_length = 0;
                let mut seq = Vec::<u8>::new();
                for obj in value {
                    let mut obj_der = obj.encode_der();
                    seq_length += obj_der.len();
                    seq.append(&mut obj_der);
                }
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | 0x20 | self.tag() ];
                ret.append(&mut ASN1::length_der(seq_length));
                ret.append(&mut seq);
                ret
            },
            ASN1::PrintableString { ref value } | ASN1::T61String { ref value } | ASN1::IA5String { ref value } => {
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | self.tag() ];
                ret.append(&mut ASN1::length_der(value.len()));
                ret.append(&mut value.to_string().into_bytes());
                ret
            },
            ASN1::UTCTime { ref value } => {
                let mut ret = vec![ ASN1_CLASS_UNIVERSAL << 6 | ASN1_TYPE_UTCTIME, 0x0D ];
                ret.append(&mut value.format("%y%m%d%H%M%SZ").to_string().into_bytes());
                ret
            },
            ASN1::ContextSpecific { tag, constructed, ref value } | ASN1::Application { tag, constructed, ref value } | ASN1::Private { tag, constructed, ref value } => {
                let constr_u8 = if constructed {
                    0x20
                } else {
                    0x00
                };
                let mut ret = vec![ self.class() << 6 | constr_u8 | tag ];
                ret.append(&mut ASN1::length_der(value.len()));
                ret.append(&mut value.to_vec());
                ret
            },
            ASN1::Unknown { class, tag, constructed, ref value } => {
                let constr_u8 = if constructed {
                    0x20
                } else {
                    0x00
                };
                let mut ret = vec![ class << 6 | constr_u8 << 5 | tag ];
                ret.append(&mut ASN1::length_der(value.len()));
                ret.append(&mut value.to_vec());
                ret
            },
        }
    }

    /**
     * Returns the DER-encoded representation of length
     *
     * # Example
     * ```
     * use asn1::ASN1;
     *
     * assert_eq!(ASN1::length_der(1203), vec![0x82, 0x04, 0xb3])
     * ```
     **/
    pub fn length_der(length: usize) -> Vec<u8> {
        if length > 127 {
            let mut ret = Vec::<u8>::new();
            let mut l = length;
            while l > 127 {
                ret.insert(0, (l & 0xFF) as u8);
                l >>= 8;
            }
            ret.insert(0, l as u8);
            let ret_l = ret.len() as u8;
            ret.insert(0, 0x80 | ret_l);
            return ret;
        } else {
            return vec![ length as u8 ];
        }
    }
}

/// Errors during decoding of DER-decoded ASN.1 messages
#[derive(PartialEq)]
pub enum DecodingError {
    IntegerTooLong { length: usize },
    InvalidClass { class: u8 },
    InvalidLength { length: usize, remaining: usize },
    InvalidObjectIdentifierFormat,
    InvalidPaddingLength{ padding_length: usize },
    InvalidUTF8 { failposition: Option<usize> },
    InvalidTimeFormat,
    NonZeroPaddingBits{ padded_byte: u8, unused: u8 },
    TypeLengthMismatch { class: u8, tag: u8, length: usize },
}

impl fmt::Debug for DecodingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DecodingError::IntegerTooLong { length } => write!(f, "Integer size of {} bytes exceeds maximum length", length),
            DecodingError::InvalidClass { class } => write!(f, "Object has invalid class '0x{:x}'", class),
            DecodingError::InvalidLength { length, remaining } => write!(f, "Invalid length: declared size of object is {} bytes, {} bytes left in input", length, remaining),
            DecodingError::InvalidObjectIdentifierFormat => write!(f, "Object identifier has invalid format"),
            DecodingError::InvalidPaddingLength{ padding_length } => write!(f, "Invalid padding length: Declared padding length is {}", padding_length),
            DecodingError::InvalidUTF8 { failposition } => { 
                if let Some(i) = failposition { 
                    write!(f, "Invalid UTF8 string: Invalid byte at string position {}", i)
                } else {
                    write!(f, "Invalid UTF8 string: Unexpected end of data")
                }
            }
            DecodingError::InvalidTimeFormat => write!(f, "Invalid DateTime format"),
            DecodingError::NonZeroPaddingBits{ padded_byte, unused } => write!(f, "Object with non-zero bit padding: padded byte is 0x{:02x} with {} unused bits", padded_byte, unused),
            DecodingError::TypeLengthMismatch { class, tag, length } => write!(f, "Invalid length {} for object with class {} and tag 0x{:02x}", length, ASN1::class_as_str(class), tag),
        }
    }
}

/// Errors during DER-encoding of ASN.1 objects
#[derive(PartialEq)]
pub enum EncodingError {
    InvalidCharacter{ character: char },
    InvalidOIDRoot{ root: u64 },
    InvalidOIDSecondLevel{ root: u64, second: u64 },
    InvalidPaddingLength{ padding_length: usize },
    InvalidToken{ value: char, allowed: Vec<char> },
}

impl fmt::Debug for EncodingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            EncodingError::InvalidCharacter{ character } => write!(f, "Character {} not allowed in this ASN.1 type", character),
            EncodingError::InvalidOIDRoot{ root } => write!(f, "Invalid OID root {}", root),
            EncodingError::InvalidOIDSecondLevel{ root, second } => write!(f, "Invalid second node {} for OID root {}", second, root),
            EncodingError::InvalidPaddingLength{ padding_length } => write!(f, "Invalid padding length '{}'", padding_length),
            EncodingError::InvalidToken{ value, ref allowed } => write!(f, "Invalid token '{}' found, tokens allowed here are: {:?}", value, allowed),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn import_complex() {
        let mut input = vec![ 0x30, 0x82, 0x05, 0x89, 0x30, 0x81, 0xcb, 0xa1, 0x64, 0xa4, 0x62, 0x30, 0x60, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x41, 0x55, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x08, 0x0c, 0x0a, 0x53, 0x6f, 0x6d, 0x65, 0x2d, 0x53, 0x74, 0x61, 0x74, 0x65, 0x31, 0x21, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c, 0x18, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x65, 0x74, 0x20, 0x57, 0x69, 0x64, 0x67, 0x69, 0x74, 0x73, 0x20, 0x50, 0x74, 0x79, 0x20, 0x4c, 0x74, 0x64, 0x31, 0x19, 0x30, 0x17, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x10, 0x4f, 0x43, 0x53, 0x50, 0x20, 0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x54, 0x65, 0x73, 0x74, 0x30, 0x3e, 0x30, 0x3c, 0x30, 0x3a, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14, 0x27, 0x56, 0xa3, 0x5b, 0x43, 0x65, 0x7b, 0x49, 0x82, 0xa4, 0xe8, 0x5b, 0x33, 0x62, 0xca, 0x33, 0xeb, 0x04, 0x69, 0x24, 0x04, 0x14, 0x0b, 0xbb, 0x1f, 0x78, 0x67, 0x77, 0x69, 0x49, 0x94, 0xb6, 0x94, 0x12, 0x8d, 0x17, 0x8e, 0xa2, 0x46, 0x80, 0xb0, 0x8c, 0x02, 0x01, 0x01, 0xa2, 0x23, 0x30, 0x21, 0x30, 0x1f, 0x06, 0x09, 0x2b, 0x06, 0x01, 0x05, 0x05, 0x07, 0x30, 0x01, 0x02, 0x04, 0x12, 0x04, 0x10, 0x88, 0x12, 0x99, 0xc6, 0x9f, 0x8f, 0xde, 0x53, 0xdf, 0x54, 0x45, 0xfe, 0x50, 0xc5, 0xf7, 0xff, 0xa0, 0x82, 0x04, 0xb7, 0x30, 0x82, 0x04, 0xb3, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00, 0x03, 0x82, 0x01, 0x01, 0x00, 0xb6, 0xc2, 0x6a, 0x94, 0xef, 0xfd, 0x80, 0x78, 0x68, 0x9b, 0x43, 0xa4, 0x0f, 0xbe, 0xa9, 0xd5, 0x52, 0x98, 0x10, 0xad, 0xde, 0xef, 0xa8, 0xef, 0x3b, 0xbb, 0xfd, 0x59, 0x45, 0xee, 0xfe, 0x2c, 0xd7, 0x8f, 0x3d, 0xff, 0xb7, 0xee, 0x1d, 0x77, 0xfd, 0xf8, 0x68, 0x15, 0x69, 0x81, 0x51, 0xa7, 0x9f, 0x42, 0x6f, 0xd0, 0x0d, 0xf7, 0xbb, 0x63, 0xac, 0xec, 0xc2, 0xb6, 0xa9, 0xa9, 0xf3, 0x3c, 0x62, 0xa3, 0xf7, 0x5a, 0x7a, 0x00, 0x01, 0xea, 0x21, 0x45, 0x0a, 0x7f, 0x67, 0xe8, 0xce, 0xe0, 0xb4, 0xfd, 0x28, 0x0b, 0x56, 0xdf, 0x47, 0x32, 0xf6, 0x71, 0xfb, 0x6f, 0x63, 0x48, 0x60, 0x92, 0x6b, 0x3f, 0x1a, 0xe7, 0x90, 0x97, 0xa8, 0x7b, 0xfd, 0x5b, 0xfc, 0x81, 0x37, 0x1e, 0x51, 0xd3, 0x09, 0x43, 0x50, 0xb4, 0x84, 0x43, 0xd3, 0xb0, 0xbe, 0xfd, 0xbb, 0x7d, 0xaf, 0xa3, 0xd8, 0x51, 0xe7, 0x64, 0x60, 0xf5, 0x5d, 0x17, 0xc4, 0xb5, 0x02, 0x4b, 0x56, 0x3e, 0xcf, 0x3c, 0x3a, 0x7d, 0x0a, 0xae, 0xfb, 0xf7, 0xa4, 0x84, 0xa2, 0x29, 0x3b, 0xf1, 0xc7, 0xfb, 0x85, 0x31, 0x63, 0xc8, 0x92, 0x7d, 0x27, 0x7f, 0x26, 0xb4, 0x5b, 0x1e, 0x84, 0x7f, 0x64, 0x42, 0x17, 0xd6, 0x31, 0x34, 0x64, 0xe4, 0x6a, 0xd1, 0x13, 0x22, 0x50, 0x9c, 0x5d, 0x09, 0x94, 0xf5, 0xe7, 0x29, 0x13, 0x67, 0x5b, 0x9f, 0xfe, 0xe1, 0x53, 0xd5, 0x81, 0x06, 0xd6, 0xb3, 0xa0, 0xec, 0xbe, 0xdc, 0x4a, 0x80, 0xb0, 0x74, 0xcc, 0xaf, 0xe3, 0x35, 0x59, 0xc6, 0x37, 0x9b, 0x6b, 0xc5, 0x64, 0x47, 0xa7, 0xe5, 0x63, 0xac, 0x13, 0x5f, 0xc3, 0x39, 0x1e, 0x74, 0x24, 0x04, 0x55, 0xd1, 0xc7, 0x2e, 0xe0, 0x31, 0x3b, 0x9c, 0xad, 0xec, 0xd8, 0x9d, 0xbf, 0xf5, 0xb0, 0x47, 0x5e, 0xd3, 0xcb, 0x15, 0xd6, 0xdb, 0xa0, 0x82, 0x03, 0x9b, 0x30, 0x82, 0x03, 0x97, 0x30, 0x82, 0x03, 0x93, 0x30, 0x82, 0x02, 0x7b, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x02, 0x09, 0x00, 0xd5, 0x1f, 0x33, 0x31, 0xb5, 0xa7, 0x41, 0xc9, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00, 0x30, 0x60, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x41, 0x55, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x08, 0x0c, 0x0a, 0x53, 0x6f, 0x6d, 0x65, 0x2d, 0x53, 0x74, 0x61, 0x74, 0x65, 0x31, 0x21, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c, 0x18, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x65, 0x74, 0x20, 0x57, 0x69, 0x64, 0x67, 0x69, 0x74, 0x73, 0x20, 0x50, 0x74, 0x79, 0x20, 0x4c, 0x74, 0x64, 0x31, 0x19, 0x30, 0x17, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x10, 0x4f, 0x43, 0x53, 0x50, 0x20, 0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x54, 0x65, 0x73, 0x74, 0x30, 0x1e, 0x17, 0x0d, 0x31, 0x38, 0x30, 0x38, 0x31, 0x37, 0x30, 0x37, 0x31, 0x30, 0x30, 0x34, 0x5a, 0x17, 0x0d, 0x31, 0x38, 0x30, 0x39, 0x31, 0x36, 0x30, 0x37, 0x31, 0x30, 0x30, 0x34, 0x5a, 0x30, 0x60, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x41, 0x55, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x08, 0x0c, 0x0a, 0x53, 0x6f, 0x6d, 0x65, 0x2d, 0x53, 0x74, 0x61, 0x74, 0x65, 0x31, 0x21, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c, 0x18, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x65, 0x74, 0x20, 0x57, 0x69, 0x64, 0x67, 0x69, 0x74, 0x73, 0x20, 0x50, 0x74, 0x79, 0x20, 0x4c, 0x74, 0x64, 0x31, 0x19, 0x30, 0x17, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x10, 0x4f, 0x43, 0x53, 0x50, 0x20, 0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x54, 0x65, 0x73, 0x74, 0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00, 0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01, 0x00, 0xbb, 0x15, 0x32, 0xae, 0x2f, 0x61, 0x09, 0x89, 0x18, 0x79, 0xcf, 0x74, 0xec, 0xeb, 0x0e, 0xa6, 0x42, 0x61, 0xfd, 0xd9, 0xc7, 0x6f, 0x2b, 0x9d, 0xc0, 0x25, 0x4d, 0x1c, 0x3f, 0xe3, 0x93, 0xf7, 0x8f, 0x8f, 0x61, 0x41, 0xee, 0x3f, 0xe6, 0x2c, 0x3b, 0xec, 0x96, 0x71, 0x9c, 0x23, 0xb8, 0x9e, 0xea, 0xa2, 0xd7, 0x02, 0x4c, 0xc1, 0xd5, 0x1d, 0x73, 0xa8, 0xdb, 0x88, 0xe8, 0x51, 0xf4, 0x9c, 0xab, 0xfe, 0x07, 0x67, 0x37, 0x64, 0x77, 0x24, 0x61, 0xaf, 0xec, 0xeb, 0x03, 0xc0, 0x3e, 0x96, 0x37, 0x58, 0x00, 0xcc, 0xeb, 0xfb, 0x95, 0xf0, 0x8e, 0xd5, 0x53, 0x68, 0xde, 0x59, 0xb1, 0x3e, 0x29, 0x73, 0x35, 0xbf, 0x9e, 0x62, 0x55, 0x8b, 0xce, 0x86, 0x3b, 0x78, 0x99, 0xeb, 0x1c, 0xd0, 0x69, 0xbd, 0xdd, 0x4c, 0xe8, 0x5e, 0xb7, 0x88, 0xe3, 0xd6, 0x5c, 0x22, 0x8e, 0xd8, 0x6f, 0xce, 0xfd, 0x18, 0x19, 0x33, 0x15, 0x30, 0x3f, 0x93, 0x19, 0xd5, 0x7d, 0xa5, 0x66, 0x4b, 0xbb, 0xf1, 0x32, 0x9e, 0x78, 0x4b, 0x9f, 0x67, 0xea, 0x75, 0xcd, 0xdc, 0x16, 0x3f, 0x58, 0xdd, 0x72, 0x50, 0x5f, 0xcd, 0xfe, 0xf0, 0x22, 0xa2, 0x3e, 0x7c, 0xa8, 0x1f, 0x38, 0xe9, 0x1e, 0x44, 0x67, 0xcb, 0xc2, 0x1c, 0xc1, 0x16, 0xd0, 0xea, 0x20, 0xac, 0x5f, 0x0d, 0x80, 0x06, 0x75, 0x22, 0xd5, 0x2f, 0x2a, 0xfd, 0x93, 0xca, 0x65, 0x52, 0xf2, 0x34, 0x5b, 0x9f, 0x81, 0x06, 0x19, 0x86, 0x7a, 0x66, 0xe0, 0x54, 0xfa, 0x5b, 0xd9, 0x7a, 0xac, 0x00, 0xe0, 0xf8, 0xd0, 0x32, 0xb3, 0xaf, 0x6b, 0x46, 0x6e, 0x70, 0x64, 0x59, 0x82, 0x4a, 0xdc, 0x6a, 0xae, 0x8a, 0xc3, 0x27, 0xdd, 0x85, 0xb2, 0x66, 0xc0, 0x77, 0xc7, 0xba, 0xa8, 0x01, 0x62, 0xe5, 0xa7, 0x0f, 0xab, 0xe9, 0xb1, 0x01, 0x9d, 0x19, 0x02, 0x03, 0x01, 0x00, 0x01, 0xa3, 0x50, 0x30, 0x4e, 0x30, 0x1d, 0x06, 0x03, 0x55, 0x1d, 0x0e, 0x04, 0x16, 0x04, 0x14, 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x1d, 0x23, 0x04, 0x18, 0x30, 0x16, 0x80, 0x14, 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95, 0x30, 0x0c, 0x06, 0x03, 0x55, 0x1d, 0x13, 0x04, 0x05, 0x30, 0x03, 0x01, 0x01, 0xff, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00, 0x03, 0x82, 0x01, 0x01, 0x00, 0x86, 0x38, 0xab, 0x37, 0xae, 0xba, 0x8c, 0xf4, 0xe6, 0xd0, 0xad, 0x06, 0x78, 0x40, 0x66, 0x5f, 0xd1, 0x09, 0xa0, 0xa0, 0x5f, 0x09, 0x0c, 0x7c, 0x8c, 0x03, 0x24, 0xf5, 0x8e, 0x7d, 0x1d, 0x2a, 0xcf, 0x7e, 0x1e, 0x17, 0xcc, 0xbd, 0x0a, 0xa9, 0x2e, 0x25, 0x33, 0xac, 0x1b, 0xbb, 0xf4, 0x60, 0x63, 0xc7, 0x58, 0xcd, 0xbe, 0xe9, 0x20, 0xd2, 0x4f, 0xca, 0x6f, 0x6f, 0xcb, 0x2c, 0xa0, 0xe4, 0x22, 0x7a, 0x20, 0xae, 0xae, 0x1b, 0x5e, 0x43, 0x84, 0xf0, 0xc3, 0x54, 0x78, 0xbf, 0x55, 0xd4, 0x98, 0xa2, 0xb0, 0x2d, 0x04, 0xc4, 0x40, 0x6c, 0xc6, 0xae, 0xc4, 0x25, 0x63, 0xd1, 0x0d, 0x2f, 0xe8, 0x01, 0xa5, 0xa6, 0x16, 0x25, 0xa7, 0xad, 0x80, 0x72, 0x9b, 0x00, 0xde, 0x07, 0xbd, 0x29, 0x12, 0xbd, 0xe4, 0xd6, 0xc5, 0xf1, 0x97, 0x50, 0xc7, 0xfc, 0x29, 0xab, 0x6d, 0xc9, 0x96, 0x67, 0xff, 0xd3, 0xf4, 0x5a, 0x2a, 0xbd, 0x35, 0x66, 0x39, 0xe8, 0xaf, 0x02, 0x64, 0x1d, 0x9f, 0x16, 0x8a, 0xd9, 0xb1, 0x6c, 0x5d, 0x17, 0x96, 0x93, 0x00, 0x0c, 0x70, 0x1e, 0xe0, 0xa5, 0x3c, 0x2f, 0xd1, 0x2d, 0xc1, 0xa3, 0x2f, 0x71, 0x3b, 0x80, 0x97, 0xc8, 0x6b, 0x2a, 0x85, 0xd7, 0x96, 0xf7, 0xf0, 0x81, 0xdc, 0x0a, 0xab, 0x58, 0x1a, 0x91, 0x61, 0x47, 0x95, 0x94, 0xe1, 0x02, 0x02, 0xe7, 0x0c, 0x81, 0xf3, 0xad, 0xf9, 0xf3, 0x2e, 0x3b, 0x58, 0xbc, 0x41, 0x36, 0x52, 0x73, 0xba, 0x80, 0x56, 0xd3, 0xd9, 0xb8, 0xb5, 0x5d, 0x17, 0xe0, 0x10, 0xb2, 0x2f, 0xd5, 0x77, 0xde, 0xb1, 0xf2, 0x18, 0xee, 0xa8, 0xbc, 0xb6, 0x47, 0xef, 0x63, 0xde, 0xc9, 0x82, 0x34, 0x67, 0xd5, 0xd0, 0xbb, 0x8b, 0xcd, 0xab, 0x06, 0x6c, 0x7b, 0xf0, 0xce, 0x08, 0x74, 0xa0, 0xba, 0x14, 0x8b, 0x59, 0x88 ];
        let output = ASN1::from_der(&mut input);
        let ocsp_request = output.unwrap();
        assert_eq!(ocsp_request.class(), ASN1_CLASS_UNIVERSAL);
        assert_eq!(ocsp_request.tag(), ASN1_TYPE_SEQUENCE);
        assert_eq!(input.is_empty(), true);

        let tbs_request;
        if let ASN1::Sequence { mut value } = ocsp_request {
            tbs_request = value.remove(0);
            assert_eq!(tbs_request.class(), ASN1_CLASS_UNIVERSAL);
            assert_eq!(tbs_request.tag(), ASN1_TYPE_SEQUENCE);

            if let ASN1::Sequence { mut value } = tbs_request {
                let requestor_name = value.remove(0);
                assert_eq!(requestor_name.class(), ASN1_CLASS_CONTEXTSPECIFIC);

                let directory_name;
                if let ASN1::ContextSpecific{ mut value, tag, .. } = requestor_name {
                    assert_eq!(tag, 1);
                    directory_name = ASN1::from_der(&mut value).unwrap();
                    assert_eq!(directory_name.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                } else {
                    panic!("Variant of requestor_name is not ASN1::ContextSpecific");
                }

                let rdn_sequence;
                if let ASN1::ContextSpecific{ mut value, tag, .. } = directory_name {
                    assert_eq!(tag, 4);
                    rdn_sequence = ASN1::from_der(&mut value).unwrap();
                    assert_eq!(rdn_sequence.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(rdn_sequence.tag(), ASN1_TYPE_SEQUENCE);
                } else {
                    panic!("Variant of directory_name is not ASN1::ContextSpecific");
                }
                
                let relative_distinguished_name;
                if let ASN1::Sequence{ mut value } = rdn_sequence {
                    if value.len() != 4 {
                        panic!("RDNASN1::Sequence has {} RelativeDishtinguishedName sets, 4 expected", value.len());
                    }
                    relative_distinguished_name = value.remove(0);
                    assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                    let atv;
                    if let ASN1::Set{ mut value } = relative_distinguished_name {
                        atv = value.remove(0);
                        assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                        if let ASN1::Sequence { mut value } = atv {
                            let oid = value.remove(0);
                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                            if let ASN1::ObjectIdentifier{ value } = oid {
                                assert_eq!(value, vec![ 2, 5, 4, 6 ]);
                            } else {
                                panic!("Variant of oid is not ASN1::ObjectIdentifier");
                            }
                            let printable_string = value.remove(0);
                            assert_eq!(printable_string.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(printable_string.tag(), ASN1_TYPE_PRINTABLESTRING);
                            if let ASN1::PrintableString{ value } = printable_string {
                                assert_eq!(value, "AU");
                            } else {
                                panic!("Variant of printable_string is not ASN1::PrintableString");
                            }
                        } else {
                            panic!("Variant of atv is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of relative_distinguished_name is not ASN1::Set");
                    }

                    let relative_distinguished_name = value.remove(0);
                    assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                    let atv;
                    if let ASN1::Set{ mut value } = relative_distinguished_name {
                        atv = value.remove(0);
                        assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                        if let ASN1::Sequence { mut value } = atv {
                            let oid = value.remove(0);
                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                            if let ASN1::ObjectIdentifier{ value } = oid {
                                assert_eq!(value, vec![ 2, 5, 4, 8 ]);
                            } else {
                                panic!("Variant of oid is not ASN1::ObjectIdentifier");
                            }
                            let utf8_string = value.remove(0);
                            assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                            if let ASN1::UTF8String{ value } = utf8_string {
                                assert_eq!(value, "Some-State");
                            } else {
                                panic!("Variant of utf8_string is not ASN1::UTF8String");
                            }
                        } else {
                            panic!("Variant of atv is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of relative_distinguished_name is not ASN1::Set");
                    }

                    let relative_distinguished_name = value.remove(0);
                    assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                    let atv;
                    if let ASN1::Set{ mut value } = relative_distinguished_name {
                        atv = value.remove(0);
                        assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                        if let ASN1::Sequence { mut value } = atv {
                            let oid = value.remove(0);
                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                            if let ASN1::ObjectIdentifier{ value } = oid {
                                assert_eq!(value, vec![ 2, 5, 4, 10 ]);
                            } else {
                                panic!("Variant of oid is not ASN1::ObjectIdentifier");
                            }
                            let utf8_string = value.remove(0);
                            assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                            if let ASN1::UTF8String{ value } = utf8_string {
                                assert_eq!(value, "Internet Widgits Pty Ltd");
                            } else {
                                panic!("Variant of utf8_string is not ASN1::UTF8String");
                            }
                        } else {
                            panic!("Variant of atv is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of relative_distinguished_name is not ASN1::Set");
                    }

                    let relative_distinguished_name = value.remove(0);
                    assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                    let atv;
                    if let ASN1::Set{ mut value } = relative_distinguished_name {
                        atv = value.remove(0);
                        assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                        if let ASN1::Sequence { mut value } = atv {
                            let oid = value.remove(0);
                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                            if let ASN1::ObjectIdentifier{ value } = oid {
                                assert_eq!(value, vec![ 2, 5, 4, 3 ]);
                            } else {
                                panic!("Variant of oid is not ASN1::ObjectIdentifier");
                            }
                            let utf8_string = value.remove(0);
                            assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                            if let ASN1::UTF8String{ value } = utf8_string {
                                assert_eq!(value, "OCSP Client Test");
                            } else {
                                panic!("Variant of utf8_string is not ASN1::UTF8String");
                            }
                        } else {
                            panic!("Variant of atv is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of relative_distinguished_name is not ASN1::Set");
                    }
                } else {
                    panic!("Variant of rdn_sequence is not ASN1::Sequence");
                }

                let request_list = value.remove(0);
                assert_eq!(request_list.class(), ASN1_CLASS_UNIVERSAL);
                assert_eq!(request_list.tag(), ASN1_TYPE_SEQUENCE);
                if let ASN1::Sequence{ mut value } = request_list {
                    let request = value.remove(0);
                    assert_eq!(request.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(request.tag(), ASN1_TYPE_SEQUENCE);
                    if let ASN1::Sequence{ mut value } = request {
                        let req_cert = value.remove(0);
                        assert_eq!(req_cert.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(req_cert.tag(), ASN1_TYPE_SEQUENCE);
                        if let ASN1::Sequence{ mut value } = req_cert {
                            let algorithm_identifier = value.remove(0);
                            assert_eq!(algorithm_identifier.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(algorithm_identifier.tag(), ASN1_TYPE_SEQUENCE);
                            if let ASN1::Sequence{ mut value } = algorithm_identifier {
                                let oid = value.remove(0);
                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                if let ASN1::ObjectIdentifier{ value } = oid {
                                    assert_eq!(value, vec![ 1, 3, 14, 3, 2, 26 ]);
                                } else {
                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                }
                                let null = value.remove(0);
                                assert_eq!(null.class(), ASN1_CLASS_UNIVERSAL);
                                assert_eq!(null.tag(), ASN1_TYPE_NULL);
                            } else {
                                panic!("Variant of algorithm_identifier is not ASN1::Sequence");
                            }

                            let issuer_name_hash = value.remove(0);
                            assert_eq!(issuer_name_hash.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(issuer_name_hash.tag(), ASN1_TYPE_OCTETSTRING);
                            if let ASN1::OctetString{ value } = issuer_name_hash {
                                assert_eq!(value, vec![ 0x27, 0x56, 0xa3, 0x5b, 0x43, 0x65, 0x7b, 0x49, 0x82, 0xa4, 0xe8, 0x5b, 0x33, 0x62, 0xca, 0x33, 0xeb, 0x04, 0x69, 0x24 ]);
                            } else {
                                panic!("Variant of issuer_name_hash is not ASN1::OctetString");
                            }

                            let issuer_key_hash = value.remove(0);
                            assert_eq!(issuer_key_hash.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(issuer_key_hash.tag(), ASN1_TYPE_OCTETSTRING);
                            if let ASN1::OctetString{ value } = issuer_key_hash {
                                assert_eq!(value, vec![ 0x0b, 0xbb, 0x1f, 0x78, 0x67, 0x77, 0x69, 0x49, 0x94, 0xb6, 0x94, 0x12, 0x8d, 0x17, 0x8e, 0xa2, 0x46, 0x80, 0xb0, 0x8c ]);
                            } else {
                                panic!("Variant of issuer_key_hash is not ASN1::OctetString");
                            }

                            let serial_number = value.remove(0);
                            assert_eq!(serial_number.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(serial_number.tag(), ASN1_TYPE_INTEGER);
                            if let ASN1::Integer{ value } = serial_number {
                                assert_eq!(value, U512::from(1));
                            } else {
                                panic!("Variant of serial_number is not ASN1::Integer");
                            }
                        } else {
                            panic!("Variant of req_cert is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of request is not ASN1::Sequence");
                    }
                } else {
                    panic!("Variant of request_list is not ASN1::Sequence");
                }

                let request_extensions = value.remove(0);
                assert_eq!(request_extensions.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                if let ASN1::ContextSpecific{ mut value, tag, .. } = request_extensions {
                    assert_eq!(tag, 2);
                    let extensions = ASN1::from_der(&mut value).unwrap();
                    assert_eq!(extensions.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(extensions.tag(), ASN1_TYPE_SEQUENCE);
                    if let ASN1::Sequence{ mut value } = extensions {
                        let extension = value.remove(0);
                        assert_eq!(extension.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(extension.tag(), ASN1_TYPE_SEQUENCE);
                        if let ASN1::Sequence{ mut value } = extension {
                            let oid = value.remove(0);
                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                            if let ASN1::ObjectIdentifier{ value } = oid {
                                assert_eq!(value, vec![ 1, 3, 6, 1, 5, 5, 7, 48, 1, 2 ]);
                            } else {
                                panic!("Variant of oid is not ASN1::ObjectIdentifier");
                            }
                            let nonce = value.remove(0);
                            assert_eq!(nonce.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(nonce.tag(), ASN1_TYPE_OCTETSTRING);
                            if let ASN1::OctetString{ value } = nonce {
                                assert_eq!(value, vec![ 0x04, 0x10, 0x88, 0x12, 0x99, 0xc6, 0x9f, 0x8f, 0xde, 0x53, 0xdf, 0x54, 0x45, 0xfe, 0x50, 0xc5, 0xf7, 0xff ]);
                            } else {
                                panic!("Variant of nonce is not ASN1::OctetString");
                            }
                        } else {
                            panic!("Variant of extension is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of extensions is not ASN1::Sequence");
                    }
                } else {
                    panic!("Variant of request_extension is not ASN1::ContextSpecific");
                }
            } else {
                panic!("Variant of tbs_request is not ASN1::Sequence");
            }

            let optional_signature = value.remove(0);
            assert_eq!(optional_signature.class(), ASN1_CLASS_CONTEXTSPECIFIC);
            if let ASN1::ContextSpecific{ mut value, tag, .. } = optional_signature {
                assert_eq!(tag, 0);
                let explicit_signature = ASN1::from_der(&mut value).unwrap();
                assert_eq!(explicit_signature.class(), ASN1_CLASS_UNIVERSAL);
                assert_eq!(explicit_signature.tag(), ASN1_TYPE_SEQUENCE);
                if let ASN1::Sequence{ mut value } = explicit_signature {
                    let algorithm_identifier = value.remove(0);
                    assert_eq!(algorithm_identifier.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(algorithm_identifier.tag(), ASN1_TYPE_SEQUENCE);
                    if let ASN1::Sequence{ mut value } = algorithm_identifier {
                        let oid = value.remove(0);
                        assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                       if let ASN1::ObjectIdentifier{ value } = oid {
                            assert_eq!(value, vec![ 1, 2, 840, 113549, 1, 1, 11 ]);
                        } else {
                            panic!("Variant of oid is not ASN1::Sequence");
                        }

                        let null = value.remove(0);
                        assert_eq!(null.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(null.tag(), ASN1_TYPE_NULL);
                    } else {
                        panic!("Variant of algorithm_identifier is not ASN1::Sequence");
                    }

                    let signature = value.remove(0);
                    assert_eq!(signature.class(), ASN1_CLASS_UNIVERSAL);
                    assert_eq!(signature.tag(), ASN1_TYPE_BITSTRING);
                    if let ASN1::BitString{ value, length } = signature {
                        assert_eq!(length, 2048);
                        assert_eq!(value, vec![ 0xb6, 0xc2, 0x6a, 0x94, 0xef, 0xfd, 0x80, 0x78, 0x68, 0x9b, 0x43, 0xa4, 0x0f, 0xbe, 0xa9, 0xd5, 0x52, 0x98, 0x10, 0xad, 0xde, 0xef, 0xa8, 0xef, 0x3b, 0xbb, 0xfd, 0x59, 0x45, 0xee, 0xfe, 0x2c, 0xd7, 0x8f, 0x3d, 0xff, 0xb7, 0xee, 0x1d, 0x77, 0xfd, 0xf8, 0x68, 0x15, 0x69, 0x81, 0x51, 0xa7, 0x9f, 0x42, 0x6f, 0xd0, 0x0d, 0xf7, 0xbb, 0x63, 0xac, 0xec, 0xc2, 0xb6, 0xa9, 0xa9, 0xf3, 0x3c, 0x62, 0xa3, 0xf7, 0x5a, 0x7a, 0x00, 0x01, 0xea, 0x21, 0x45, 0x0a, 0x7f, 0x67, 0xe8, 0xce, 0xe0, 0xb4, 0xfd, 0x28, 0x0b, 0x56, 0xdf, 0x47, 0x32, 0xf6, 0x71, 0xfb, 0x6f, 0x63, 0x48, 0x60, 0x92, 0x6b, 0x3f, 0x1a, 0xe7, 0x90, 0x97, 0xa8, 0x7b, 0xfd, 0x5b, 0xfc, 0x81, 0x37, 0x1e, 0x51, 0xd3, 0x09, 0x43, 0x50, 0xb4, 0x84, 0x43, 0xd3, 0xb0, 0xbe, 0xfd, 0xbb, 0x7d, 0xaf, 0xa3, 0xd8, 0x51, 0xe7, 0x64, 0x60, 0xf5, 0x5d, 0x17, 0xc4, 0xb5, 0x02, 0x4b, 0x56, 0x3e, 0xcf, 0x3c, 0x3a, 0x7d, 0x0a, 0xae, 0xfb, 0xf7, 0xa4, 0x84, 0xa2, 0x29, 0x3b, 0xf1, 0xc7, 0xfb, 0x85, 0x31, 0x63, 0xc8, 0x92, 0x7d, 0x27, 0x7f, 0x26, 0xb4, 0x5b, 0x1e, 0x84, 0x7f, 0x64, 0x42, 0x17, 0xd6, 0x31, 0x34, 0x64, 0xe4, 0x6a, 0xd1, 0x13, 0x22, 0x50, 0x9c, 0x5d, 0x09, 0x94, 0xf5, 0xe7, 0x29, 0x13, 0x67, 0x5b, 0x9f, 0xfe, 0xe1, 0x53, 0xd5, 0x81, 0x06, 0xd6, 0xb3, 0xa0, 0xec, 0xbe, 0xdc, 0x4a, 0x80, 0xb0, 0x74, 0xcc, 0xaf, 0xe3, 0x35, 0x59, 0xc6, 0x37, 0x9b, 0x6b, 0xc5, 0x64, 0x47, 0xa7, 0xe5, 0x63, 0xac, 0x13, 0x5f, 0xc3, 0x39, 0x1e, 0x74, 0x24, 0x04, 0x55, 0xd1, 0xc7, 0x2e, 0xe0, 0x31, 0x3b, 0x9c, 0xad, 0xec, 0xd8, 0x9d, 0xbf, 0xf5, 0xb0, 0x47, 0x5e, 0xd3, 0xcb, 0x15, 0xd6, 0xdb ]);
                    } else {
                        panic!("Variant of signature is not ASN1::BitString");
                    }

                    let certs = value.remove(0);
                    assert_eq!(certs.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                    if let ASN1::ContextSpecific{ mut value, tag, .. } = certs {
                        assert_eq!(tag, 0); // of certs
                        let certificate_sequence = ASN1::from_der(&mut value).unwrap();
                        assert_eq!(certificate_sequence.class(), ASN1_CLASS_UNIVERSAL);
                        assert_eq!(certificate_sequence.tag(), ASN1_TYPE_SEQUENCE);
                        if let ASN1::Sequence{ mut value } = certificate_sequence {
                            let certificate = value.remove(0);
                            assert_eq!(certificate.class(), ASN1_CLASS_UNIVERSAL);
                            assert_eq!(certificate.tag(), ASN1_TYPE_SEQUENCE);
                            if let ASN1::Sequence{ mut value } = certificate {
                                let tbs_certificate = value.remove(0);
                                assert_eq!(tbs_certificate.class(), ASN1_CLASS_UNIVERSAL);
                                assert_eq!(tbs_certificate.tag(), ASN1_TYPE_SEQUENCE);
                                if let ASN1::Sequence{ mut value } = tbs_certificate {
                                    let version = value.remove(0);
                                    assert_eq!(version.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                                    if let ASN1::ContextSpecific{ mut value, tag, .. } = version {
                                        assert_eq!(tag, 0);
                                        let version_int = ASN1::from_der(&mut value).unwrap();
                                        assert_eq!(version_int.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(version_int.tag(), ASN1_TYPE_INTEGER);
                                        if let ASN1::Integer{ value } = version_int {
                                            assert_eq!(value, U512::from(2));
                                        } else {
                                            panic!("Variant of version_int is not ASN1::Integer");
                                        }
                                    } else {
                                        panic!("Variant of version is not ASN1::ContextSpecific");
                                    }

                                    let serial_number = value.remove(0);
                                    assert_eq!(serial_number.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(serial_number.tag(), ASN1_TYPE_INTEGER);
                                    if let ASN1::Integer{ value } = serial_number {
                                        assert_eq!(value, U512::from(15357049542950732233 as u64));
                                    } else {
                                        panic!("Variant of serial_number is not ASN1::Integer");
                                    }

                                    let algorithm_identifier = value.remove(0);
                                    assert_eq!(algorithm_identifier.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(algorithm_identifier.tag(), ASN1_TYPE_SEQUENCE);
                                    if let ASN1::Sequence{ mut value } = algorithm_identifier {
                                        let oid = value.remove(0);
                                        assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                        if let ASN1::ObjectIdentifier{ value } = oid {
                                            assert_eq!(value, vec![ 1, 2, 840, 113549, 1, 1, 11 ]);
                                        } else {
                                            panic!("Variant of oid is not ASN1::Sequence");
                                        }

                                        let null = value.remove(0);
                                        assert_eq!(null.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(null.tag(), ASN1_TYPE_NULL);
                                    } else {
                                        panic!("Variant of algorithm_identifier is not ASN1::Sequence");
                                    }
                                    
                                    let issuer = value.remove(0);
                                    assert_eq!(issuer.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(issuer.tag(), ASN1_TYPE_SEQUENCE);
                                    if let ASN1::Sequence{ mut value } = issuer {
                                        if value.len() != 4 {
                                            panic!("RDNASN1::Sequence has {} RelativeDishtinguishedName sets, 4 expected", value.len());
                                        }
                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 6 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let printable_string = value.remove(0);
                                                assert_eq!(printable_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(printable_string.tag(), ASN1_TYPE_PRINTABLESTRING);
                                                if let ASN1::PrintableString{ value } = printable_string {
                                                    assert_eq!(value, "AU");
                                                } else {
                                                    panic!("Variant of printable_string is not ASN1::PrintableString");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 8 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "Some-State");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 10 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "Internet Widgits Pty Ltd");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 3 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "OCSP Client Test");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }
                                    } else {
                                        panic!("Variant of issuer is not ASN1::Sequence");
                                    }

                                    let validity = value.remove(0);
                                    assert_eq!(validity.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(validity.tag(), ASN1_TYPE_SEQUENCE);
                                    if let ASN1::Sequence{ mut value } = validity {
                                        let not_before = value.remove(0);
                                        assert_eq!(not_before.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(not_before.tag(), ASN1_TYPE_UTCTIME);
                                        if let ASN1::UTCTime{ value } = not_before {
                                            assert_eq!(value, chrono::Utc.ymd(2018, 8, 17).and_hms(7, 10, 4));
                                        } else {
                                            panic!("Variant of not_before is not ASN1::UTCTime");
                                        }

                                        let not_after = value.remove(0);
                                        assert_eq!(not_after.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(not_after.tag(), ASN1_TYPE_UTCTIME);
                                        if let ASN1::UTCTime{ value } = not_after {
                                            assert_eq!(value, chrono::Utc.ymd(2018, 9, 16).and_hms(7, 10, 4));
                                        } else {
                                            panic!("Variant of not_after is not ASN1::UTCTime");
                                        }
                                    } else {
                                        panic!("Variant of validity is not ASN1::Sequence");
                                    }

                                    let subject = value.remove(0);
                                    if let ASN1::Sequence{ mut value } = subject {
                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 6 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let printable_string = value.remove(0);
                                                assert_eq!(printable_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(printable_string.tag(), ASN1_TYPE_PRINTABLESTRING);
                                                if let ASN1::PrintableString{ value } = printable_string {
                                                    assert_eq!(value, "AU");
                                                } else {
                                                    panic!("Variant of printable_string is not ASN1::PrintableString");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 8 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "Some-State");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 10 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "Internet Widgits Pty Ltd");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }

                                        let relative_distinguished_name = value.remove(0);
                                        assert_eq!(relative_distinguished_name.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(relative_distinguished_name.tag(), ASN1_TYPE_SET);

                                        let atv;
                                        if let ASN1::Set{ mut value } = relative_distinguished_name {
                                            atv = value.remove(0);
                                            assert_eq!(atv.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(atv.tag(), ASN1_TYPE_SEQUENCE);

                                            if let ASN1::Sequence { mut value } = atv {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 4, 3 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }
                                                let utf8_string = value.remove(0);
                                                assert_eq!(utf8_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(utf8_string.tag(), ASN1_TYPE_UTF8STRING);
                                                if let ASN1::UTF8String{ value } = utf8_string {
                                                    assert_eq!(value, "OCSP Client Test");
                                                } else {
                                                    panic!("Variant of utf8_string is not ASN1::UTF8String");
                                                }
                                            } else {
                                                panic!("Variant of atv is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of relative_distinguished_name is not ASN1::Set");
                                        }
                                    } else {
                                        panic!("Variant of subject is not ASN1::Sequence");
                                    }

                                    let subject_public_key_info = value.remove(0);
                                    assert_eq!(subject_public_key_info.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(subject_public_key_info.tag(), ASN1_TYPE_SEQUENCE);
                                    if let ASN1::Sequence{ mut value } = subject_public_key_info {
                                        let algorithm_identifier = value.remove(0);
                                        assert_eq!(algorithm_identifier.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(algorithm_identifier.tag(), ASN1_TYPE_SEQUENCE);
                                        if let ASN1::Sequence{ mut value } = algorithm_identifier {
                                            let oid = value.remove(0);
                                            assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                            if let ASN1::ObjectIdentifier{ value } = oid {
                                                assert_eq!(value, vec![ 1, 2, 840, 113549, 1, 1, 1 ]);
                                            } else {
                                                panic!("Variant of oid is not ASN1::Sequence");
                                            }

                                            let null = value.remove(0);
                                            assert_eq!(null.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(null.tag(), ASN1_TYPE_NULL);
                                        } else {
                                            panic!("Variant of algorithm_identifier is not ASN1::Sequence");
                                        }

                                        let subject_public_key = value.remove(0);
                                        assert_eq!(subject_public_key.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(subject_public_key.tag(), ASN1_TYPE_BITSTRING);
                                        if let ASN1::BitString{ length, value } = subject_public_key {
                                            assert_eq!(length, 2160);
                                            // Subject public key: RSA exponent and modulus encoded
                                            // into bitstring
                                            // we'll still interpret this as an bit string as the
                                            // modulus is 257 byte in size and exceeds U512,
                                            // therefore produces a parsing error
                                            assert_eq!(value, vec![ 0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01, 0x00, 0xbb, 0x15, 0x32, 0xae, 0x2f, 0x61, 0x09, 0x89, 0x18, 0x79, 0xcf, 0x74, 0xec, 0xeb, 0x0e, 0xa6, 0x42, 0x61, 0xfd, 0xd9, 0xc7, 0x6f, 0x2b, 0x9d, 0xc0, 0x25, 0x4d, 0x1c, 0x3f, 0xe3, 0x93, 0xf7, 0x8f, 0x8f, 0x61, 0x41, 0xee, 0x3f, 0xe6, 0x2c, 0x3b, 0xec, 0x96, 0x71, 0x9c, 0x23, 0xb8, 0x9e, 0xea, 0xa2, 0xd7, 0x02, 0x4c, 0xc1, 0xd5, 0x1d, 0x73, 0xa8, 0xdb, 0x88, 0xe8, 0x51, 0xf4, 0x9c, 0xab, 0xfe, 0x07, 0x67, 0x37, 0x64, 0x77, 0x24, 0x61, 0xaf, 0xec, 0xeb, 0x03, 0xc0, 0x3e, 0x96, 0x37, 0x58, 0x00, 0xcc, 0xeb, 0xfb, 0x95, 0xf0, 0x8e, 0xd5, 0x53, 0x68, 0xde, 0x59, 0xb1, 0x3e, 0x29, 0x73, 0x35, 0xbf, 0x9e, 0x62, 0x55, 0x8b, 0xce, 0x86, 0x3b, 0x78, 0x99, 0xeb, 0x1c, 0xd0, 0x69, 0xbd, 0xdd, 0x4c, 0xe8, 0x5e, 0xb7, 0x88, 0xe3, 0xd6, 0x5c, 0x22, 0x8e, 0xd8, 0x6f, 0xce, 0xfd, 0x18, 0x19, 0x33, 0x15, 0x30, 0x3f, 0x93, 0x19, 0xd5, 0x7d, 0xa5, 0x66, 0x4b, 0xbb, 0xf1, 0x32, 0x9e, 0x78, 0x4b, 0x9f, 0x67, 0xea, 0x75, 0xcd, 0xdc, 0x16, 0x3f, 0x58, 0xdd, 0x72, 0x50, 0x5f, 0xcd, 0xfe, 0xf0, 0x22, 0xa2, 0x3e, 0x7c, 0xa8, 0x1f, 0x38, 0xe9, 0x1e, 0x44, 0x67, 0xcb, 0xc2, 0x1c, 0xc1, 0x16, 0xd0, 0xea, 0x20, 0xac, 0x5f, 0x0d, 0x80, 0x06, 0x75, 0x22, 0xd5, 0x2f, 0x2a, 0xfd, 0x93, 0xca, 0x65, 0x52, 0xf2, 0x34, 0x5b, 0x9f, 0x81, 0x06, 0x19, 0x86, 0x7a, 0x66, 0xe0, 0x54, 0xfa, 0x5b, 0xd9, 0x7a, 0xac, 0x00, 0xe0, 0xf8, 0xd0, 0x32, 0xb3, 0xaf, 0x6b, 0x46, 0x6e, 0x70, 0x64, 0x59, 0x82, 0x4a, 0xdc, 0x6a, 0xae, 0x8a, 0xc3, 0x27, 0xdd, 0x85, 0xb2, 0x66, 0xc0, 0x77, 0xc7, 0xba, 0xa8, 0x01, 0x62, 0xe5, 0xa7, 0x0f, 0xab, 0xe9, 0xb1, 0x01, 0x9d, 0x19, 0x02, 0x03, 0x01, 0x00, 0x01 ]);
                                        } else {
                                            panic!("Variant of subject_public_key is not ASN1::BitString");
                                        }
                                    } else {
                                        panic!("Variant of subject_public_key_info is not ASN1::Sequence");
                                    }

                                    let extensions = value.remove(0);
                                    assert_eq!(extensions.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                                    if let ASN1::ContextSpecific{ mut value, tag, .. } = extensions {
                                        assert_eq!(tag, 3);
                                        let extensions_sequence = ASN1::from_der(&mut value).unwrap();
                                        assert_eq!(extensions_sequence.class(), ASN1_CLASS_UNIVERSAL);
                                        assert_eq!(extensions_sequence.tag(), ASN1_TYPE_SEQUENCE);
                                        if let ASN1::Sequence{ mut value } = extensions_sequence {
                                            let extension = value.remove(0);
                                            assert_eq!(extension.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(extension.tag(), ASN1_TYPE_SEQUENCE);
                                            if let ASN1::Sequence{ mut value } = extension {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 29, 14 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }

                                                let octet_string = value.remove(0);
                                                assert_eq!(octet_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(octet_string.tag(), ASN1_TYPE_OCTETSTRING);
                                                if let ASN1::OctetString { mut value } = octet_string {
                                                    let subject_key_info = ASN1::from_der(&mut value).unwrap();
                                                    assert_eq!(subject_key_info.class(), ASN1_CLASS_UNIVERSAL);
                                                    assert_eq!(subject_key_info.tag(), ASN1_TYPE_OCTETSTRING);
                                                    if let ASN1::OctetString { value } = subject_key_info {
                                                        assert_eq!(value, vec![ 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]);
                                                    } else {
                                                        panic!("Variant of subject_key_info is not ASN1::OctetString");
                                                    }
                                                } else {
                                                    panic!("Variant of octet_string is not ASN1::OctetString");
                                                }
                                            } else {
                                                panic!("Variant of extension is not ASN1::Sequence");
                                            }

                                            let extension = value.remove(0);
                                            assert_eq!(extension.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(extension.tag(), ASN1_TYPE_SEQUENCE);
                                            if let ASN1::Sequence{ mut value } = extension {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 29, 35 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }

                                                let octet_string = value.remove(0);
                                                assert_eq!(octet_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(octet_string.tag(), ASN1_TYPE_OCTETSTRING);
                                                if let ASN1::OctetString { mut value } = octet_string {
                                                    let key_identifier_seq = ASN1::from_der(&mut value).unwrap();
                                                    assert_eq!(key_identifier_seq.class(), ASN1_CLASS_UNIVERSAL);
                                                    assert_eq!(key_identifier_seq.tag(), ASN1_TYPE_SEQUENCE);
                                                    if let ASN1::Sequence{ mut value } = key_identifier_seq {
                                                        let key_identifier = value.remove(0);
                                                        assert_eq!(key_identifier.class(), ASN1_CLASS_CONTEXTSPECIFIC);
                                                        if let ASN1::ContextSpecific{ value, tag, .. } = key_identifier {
                                                            assert_eq!(tag, 0);
                                                            // Octet string implicitly wrapped in
                                                            // Context-Specific
                                                            assert_eq!(value, vec![ 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]);
                                                        } else {
                                                            panic!("Variant of key_identifier is not ASN1::ContextSpecific");
                                                        }
                                                    } else {
                                                        panic!("Variant of key_identifier_seq is not ASN1::Sequence");
                                                    }
                                                } else {
                                                    panic!("Variant of octet_string is not ASN1::OctetString");
                                                }
                                            } else {
                                                panic!("Variant of extension is not ASN1::Sequence");
                                            }

                                            let extension = value.remove(0);
                                            assert_eq!(extension.class(), ASN1_CLASS_UNIVERSAL);
                                            assert_eq!(extension.tag(), ASN1_TYPE_SEQUENCE);
                                            if let ASN1::Sequence{ mut value } = extension {
                                                let oid = value.remove(0);
                                                assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                                if let ASN1::ObjectIdentifier{ value } = oid {
                                                    assert_eq!(value, vec![ 2, 5, 29, 19 ]);
                                                } else {
                                                    panic!("Variant of oid is not ASN1::ObjectIdentifier");
                                                }

                                                let octet_string = value.remove(0);
                                                assert_eq!(octet_string.class(), ASN1_CLASS_UNIVERSAL);
                                                assert_eq!(octet_string.tag(), ASN1_TYPE_OCTETSTRING);
                                                if let ASN1::OctetString { mut value } = octet_string {
                                                    let ca_constraint_seq = ASN1::from_der(&mut value).unwrap();
                                                    assert_eq!(ca_constraint_seq.class(), ASN1_CLASS_UNIVERSAL);
                                                    assert_eq!(ca_constraint_seq.tag(), ASN1_TYPE_SEQUENCE);
                                                    if let ASN1::Sequence{ mut value } = ca_constraint_seq {
                                                        let ca_constraint = value.remove(0);
                                                        assert_eq!(ca_constraint.class(), ASN1_CLASS_UNIVERSAL);
                                                        assert_eq!(ca_constraint.tag(), ASN1_TYPE_BOOLEAN);
                                                        if let ASN1::Boolean{ value } = ca_constraint {
                                                            assert_eq!(value, true);
                                                        } else {
                                                            panic!("Variant of ca_constraint is not ASN1::Boolean");
                                                        }
                                                    } else {
                                                        panic!("Variant of ca_constraint_seq is not ASN1::Sequence");
                                                    }
                                                } else {
                                                    panic!("Variant of octet_string is not ASN1::OctetString");
                                                }
                                            } else {
                                                panic!("Variant of extension is not ASN1::Sequence");
                                            }
                                        } else {
                                            panic!("Variant of extensions_sequence is not ASN1::Sequence");
                                        }
                                    } else {
                                        panic!("Variant of extensions is not ASN1::ContextSpecific");
                                    }
                                } else {
                                    panic!("Variant of tbs_certificate is not ASN1::Sequence");
                                }

                                let algorithm_identifier = value.remove(0);
                                assert_eq!(algorithm_identifier.class(), ASN1_CLASS_UNIVERSAL);
                                assert_eq!(algorithm_identifier.tag(), ASN1_TYPE_SEQUENCE);
                                if let ASN1::Sequence{ mut value } = algorithm_identifier {
                                    let oid = value.remove(0);
                                    assert_eq!(oid.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(oid.tag(), ASN1_TYPE_OBJECTIDENTIFIER);
                                    if let ASN1::ObjectIdentifier{ value } = oid {
                                        assert_eq!(value, vec![ 1, 2, 840, 113549, 1, 1, 11 ]);
                                    } else {
                                        panic!("Variant of oid is not ASN1::Sequence");
                                    }

                                    let null = value.remove(0);
                                    assert_eq!(null.class(), ASN1_CLASS_UNIVERSAL);
                                    assert_eq!(null.tag(), ASN1_TYPE_NULL);
                                } else {
                                    panic!("Variant of algorithm_identifier is not ASN1::Sequence");
                                }

                                let signature = value.remove(0);
                                assert_eq!(signature.class(), ASN1_CLASS_UNIVERSAL);
                                assert_eq!(signature.tag(), ASN1_TYPE_BITSTRING);
                                if let ASN1::BitString{ value, length } = signature {
                                    assert_eq!(length, 2048);
                                    assert_eq!(value, vec![ 0x86, 0x38, 0xab, 0x37, 0xae, 0xba, 0x8c, 0xf4, 0xe6, 0xd0, 0xad, 0x06, 0x78, 0x40, 0x66, 0x5f, 0xd1, 0x09, 0xa0, 0xa0, 0x5f, 0x09, 0x0c, 0x7c, 0x8c, 0x03, 0x24, 0xf5, 0x8e, 0x7d, 0x1d, 0x2a, 0xcf, 0x7e, 0x1e, 0x17, 0xcc, 0xbd, 0x0a, 0xa9, 0x2e, 0x25, 0x33, 0xac, 0x1b, 0xbb, 0xf4, 0x60, 0x63, 0xc7, 0x58, 0xcd, 0xbe, 0xe9, 0x20, 0xd2, 0x4f, 0xca, 0x6f, 0x6f, 0xcb, 0x2c, 0xa0, 0xe4, 0x22, 0x7a, 0x20, 0xae, 0xae, 0x1b, 0x5e, 0x43, 0x84, 0xf0, 0xc3, 0x54, 0x78, 0xbf, 0x55, 0xd4, 0x98, 0xa2, 0xb0, 0x2d, 0x04, 0xc4, 0x40, 0x6c, 0xc6, 0xae, 0xc4, 0x25, 0x63, 0xd1, 0x0d, 0x2f, 0xe8, 0x01, 0xa5, 0xa6, 0x16, 0x25, 0xa7, 0xad, 0x80, 0x72, 0x9b, 0x00, 0xde, 0x07, 0xbd, 0x29, 0x12, 0xbd, 0xe4, 0xd6, 0xc5, 0xf1, 0x97, 0x50, 0xc7, 0xfc, 0x29, 0xab, 0x6d, 0xc9, 0x96, 0x67, 0xff, 0xd3, 0xf4, 0x5a, 0x2a, 0xbd, 0x35, 0x66, 0x39, 0xe8, 0xaf, 0x02, 0x64, 0x1d, 0x9f, 0x16, 0x8a, 0xd9, 0xb1, 0x6c, 0x5d, 0x17, 0x96, 0x93, 0x00, 0x0c, 0x70, 0x1e, 0xe0, 0xa5, 0x3c, 0x2f, 0xd1, 0x2d, 0xc1, 0xa3, 0x2f, 0x71, 0x3b, 0x80, 0x97, 0xc8, 0x6b, 0x2a, 0x85, 0xd7, 0x96, 0xf7, 0xf0, 0x81, 0xdc, 0x0a, 0xab, 0x58, 0x1a, 0x91, 0x61, 0x47, 0x95, 0x94, 0xe1, 0x02, 0x02, 0xe7, 0x0c, 0x81, 0xf3, 0xad, 0xf9, 0xf3, 0x2e, 0x3b, 0x58, 0xbc, 0x41, 0x36, 0x52, 0x73, 0xba, 0x80, 0x56, 0xd3, 0xd9, 0xb8, 0xb5, 0x5d, 0x17, 0xe0, 0x10, 0xb2, 0x2f, 0xd5, 0x77, 0xde, 0xb1, 0xf2, 0x18, 0xee, 0xa8, 0xbc, 0xb6, 0x47, 0xef, 0x63, 0xde, 0xc9, 0x82, 0x34, 0x67, 0xd5, 0xd0, 0xbb, 0x8b, 0xcd, 0xab, 0x06, 0x6c, 0x7b, 0xf0, 0xce, 0x08, 0x74, 0xa0, 0xba, 0x14, 0x8b, 0x59, 0x88 ]);
                                } else {
                                    panic!("Variant of signature is not ASN1::BitString");
                                }
                            } else {
                                panic!("Variant of certificate is not ASN1::Sequence");
                            }
                        } else {
                            panic!("Variant of certificate_sequence is not ASN1::Sequence");
                        }
                    } else {
                        panic!("Variant of certs is not ASN1::ContextSpecific");
                    }
                } else {
                    panic!("Variant of explicit_signature is not ASN1::Sequence");
                }
            } else {
                panic!("Variant of optional_signature is not ASN1::Sequence");
            }
        } else {
            panic!("Variant of ocsp_request is not ASN1::Sequence");
        }
    }

    #[test]
    fn export_bool() {
        let asn1_bool = ASN1::boolean(true);
        assert_eq!(asn1_bool.unwrap().encode_der(), vec![ 0x01, 0x01, 0xff ]);
    }

    #[test]
    fn export_int() {
        let asn1_int = ASN1::integer(U512::from_dec_str("12345678901234567890").unwrap());
        assert_eq!(asn1_int.unwrap().encode_der(), vec![ 0x02, 0x08, 0xab, 0x54, 0xa9, 0x8c, 0xeb, 0x1f, 0x0a, 0xd2 ]);
    }

    #[test]
    fn export_bitstring() {
        let asn1_bitstring = ASN1::bitstring(vec![ 0xff, 0xaa, 0xe6, 0x68 ], 3); // 11111111 10101010 11100110 01101000
        assert_eq!(asn1_bitstring.unwrap().encode_der(), vec![ 0x03, 0x05, 0x03, 0xff, 0xaa, 0xe6, 0x68 ]);
    }

    #[test]
    fn export_octetstring() {
        let asn1_octetstring = ASN1::octetstring(vec![ 0x01, 0x02, 0x03, 0x04, 0x05 ]);
        assert_eq!(asn1_octetstring.unwrap().encode_der(), vec![ 0x04, 0x05, 0x01, 0x02, 0x03, 0x04, 0x05 ]);
    }

    #[test]
    fn export_null() {
        let asn1_null = ASN1::null();
        assert_eq!(asn1_null.unwrap().encode_der(), vec![ 0x05, 0x00 ]);
    }

    #[test]
    fn export_oid() {
        let asn1_oid = ASN1::objectidentifier(vec![ 1, 2, 840, 113549, 1, 1, 11 ]);
        assert_eq!(asn1_oid.unwrap().encode_der(), vec![ 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b ]);
    }

    #[test]
    fn export_utf8string() {
        let asn1_utf8 = ASN1::utf8string(String::from("ÇÐǯ"));
        assert_eq!(asn1_utf8.unwrap().encode_der(), vec![ 0x0C, 0x06, 0xc3, 0x87, 0xc3, 0x90, 0xc7, 0xaf ]);
    }

    #[test]
    fn export_sequence() {
        let asn1_bool = ASN1::boolean(true).unwrap();
        let asn1_int = ASN1::integer(U512::from_dec_str("12345678901234567890").unwrap()).unwrap();
        let asn1_oid = ASN1::objectidentifier(vec![ 1, 2, 840, 113549, 1, 1, 11 ]).unwrap();
        let asn1_seq = ASN1::sequence(vec![ asn1_bool, asn1_int, asn1_oid ]).unwrap();
        assert_eq!(asn1_seq.encode_der(), vec![ 0x30, 0x18, 0x01, 0x01, 0xff, 0x02, 0x08, 0xab, 0x54, 0xa9, 0x8c, 0xeb, 0x1f, 0x0a, 0xd2, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b ]);
    }

    #[test]
    fn export_set() {
        let asn1_utf8 = ASN1::utf8string(String::from("Internet Widgits Pty Ltd")).unwrap();
        let asn1_oid = ASN1::objectidentifier(vec![ 2, 5, 4, 10 ]).unwrap();
        let asn1_seq = ASN1::sequence(vec![ asn1_oid, asn1_utf8 ]).unwrap();
        let asn1_set = ASN1::set(vec![ asn1_seq ]).unwrap();
        assert_eq!(asn1_set.encode_der(), vec![ 0x31, 0x21, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c, 0x18, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x65, 0x74, 0x20, 0x57, 0x69, 0x64, 0x67, 0x69, 0x74, 0x73, 0x20, 0x50, 0x74, 0x79, 0x20, 0x4c, 0x74, 0x64 ]);
    }

    #[test]
    fn export_printable() {
        let asn1_printable = ASN1::printablestring(String::from("AU"));
        assert_eq!(asn1_printable.unwrap().encode_der(), vec![ 0x13, 0x02, 0x41, 0x55 ]);
    }

    #[test]
    fn export_t61() {
        let asn1_t61 = ASN1::t61String(String::from("T61 test"));
        assert_eq!(asn1_t61.unwrap().encode_der(), vec![ 0x14, 0x08, 0x54, 0x36, 0x31, 0x20, 0x74, 0x65, 0x73, 0x74 ]);
    }

    #[test]
    fn export_ia5() {
        let asn1_ia5 = ASN1::ia5String(String::from("IA5 test"));
        assert_eq!(asn1_ia5.unwrap().encode_der(), vec![ 0x16, 0x08, 0x49, 0x41, 0x35, 0x20, 0x74, 0x65, 0x73, 0x74 ]);
    }

    #[test]
    fn export_time() {
        let asn1_time = ASN1::utctime(Utc.ymd(2018, 09, 12).and_hms(08, 23, 15));
        assert_eq!(asn1_time.unwrap().encode_der(), vec![ 0x17, 0x0d, 0x31, 0x38, 0x30, 0x39, 0x31, 0x32, 0x30, 0x38, 0x32, 0x33, 0x31, 0x35, 0x5a ]);
    }

    #[test]
    fn export_contextspecific_explicit() {
        let asn1_oid = ASN1::objectidentifier(vec![ 1, 2, 840, 113549, 1, 1, 11 ]).unwrap();
        let asn1_null = ASN1::null().unwrap();
        let asn1_seq = ASN1::sequence(vec![ asn1_oid, asn1_null ]).unwrap();
        let asn1_context = ASN1::contextspecific(0x03, true, asn1_seq.encode_der()).unwrap();
        assert_eq!(asn1_context.encode_der(), vec![ 0xa3, 0x0f, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00 ]);
    }

    #[test]
    fn export_contextspecific_implicit() {
        let asn1_context = ASN1::contextspecific(0x02, false, vec![ 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]); // implicit octet string
        assert_eq!(asn1_context.unwrap().encode_der(), vec![ 0x82, 0x14, 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]);
    }

    #[test]
    fn export_application() {
        let asn1_application = ASN1::application(0x00, false, vec![ 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]); // implicit octet string
        assert_eq!(asn1_application.unwrap().encode_der(), vec![ 0x40, 0x14, 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]);
    }

    #[test]
    fn export_private() {
        let asn1_private = ASN1::private(0x00, false, vec![ 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]); // implicit octet string
        assert_eq!(asn1_private.unwrap().encode_der(), vec![ 0xc0, 0x14, 0xd8, 0xc7, 0x41, 0xbf, 0x8b, 0x9c, 0xca, 0x2c, 0x73, 0xd4, 0x2d, 0xc2, 0xb0, 0xbd, 0x23, 0xc5, 0x7e, 0x52, 0x46, 0x95 ]);
    }

    #[test]
    fn import_oid_invalid() {
        let mut oid1 = vec![ 0x06u8, 0x03u8, 0x2au8, 0x01u8, 0x80u8 ]; // last octet is invalid
        let mut oid2 = vec![ 0x06u8, 0x03u8, 0x2au8, 0x01u8, 0x81u8 ]; // last octet makes incomplete value
        assert_eq!(ASN1::from_der(&mut oid1).unwrap_err(), DecodingError::InvalidObjectIdentifierFormat{ });
        assert_eq!(ASN1::from_der(&mut oid2).unwrap_err(), DecodingError::InvalidObjectIdentifierFormat{ });
    }
}
